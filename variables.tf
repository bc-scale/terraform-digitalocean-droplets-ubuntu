// these must be modified or set at runtime
variable "do_token" {}
variable "ssh_user" {}
variable "ssh_key_path" {}
variable "vm_ssh_key_ids" { type = "list" }

// sane/cheap defaults from here-on out
variable "vm_num_of_droplets" {
  // if value of '2' is given, the VM_NAME var will be appended with '-0'
  // for the first server, then the second server with '-1', etc
  default = 1
}
variable "vm_image" {
  default = "ubuntu-18-04-x64"
}
variable "vm_region" {
  default = "sfo2"
}
variable "vm_size" {
  default = "s-1vcpu-1gb"
}
variable "vm_private_networking" {
  default = 1
}
variable "vm_backups" {
  default = 0
}
variable "vm_monitoring" {
  default = 1
}
variable "vm_ipv6" {
  default = 1
}
variable "vm_name" {
  default = "terraform-digitalocean-droplets-ubuntu"
}
// variable "vm_tags" {
//  default = ["test-tag"]
// }
// variable "vm_volume_ids" {
//   default = []
// }
